import java.util.Scanner;

public class LuckyCardGameApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		//Welcome Message
		System.out.println("----------------------------------------------------------");
		System.out.println("Welcome to LUCKY CARD GAME!!!");
		System.out.println("Basic Control:" + "\n" + "n: next");
		System.out.println("----------------------------------------------------------" + "\n");
		int points = 0;
		GameManager manager = new GameManager();
		
		while(manager.getNumberOfCards() != 0 && points <= 5) {
			System.out.println(manager);
			points += manager.calculatePoints();
			
			if (manager.calculatePoints() > 0) {
				System.out.println("Points Gained: " + manager.calculatePoints());
			}
			else {
				System.out.println("Points Lost: -1");
			}
			System.out.println("Your Current Points: " + points);
			System.out.println("Draws Left: " + manager.getNumberOfCards()/2 + "\n");
			
			manager.dealcards();
			
			boolean next = false;
			while (next == false && points <5){
				System.out.println("Type 'n' then ENTER to draw again");
				String input = reader.nextLine();
				
				if (input.equals("n")){
					next = true;
				}
				else{
					System.out.println("Invalid Input");
				}
			}
			
			System.out.println("----------------------------------------------------------" + "\n");
		}
		
		if (points >= 5)
			System.out.println("Congrats You WON!!!");
		else
			System.out.println("You LOST, Good Luck Next Time!");
		
		System.out.println("You Had: " + points + " points.");
	}
}