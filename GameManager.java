public class GameManager{
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager(){
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	
	public String toString(){
		return "Center Card: " + this.centerCard.toString() + "\n" + "Player Card: " + this.playerCard.toString() + "\n";
	}
	
	public void dealcards(){
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	public int getNumberOfCards(){
		return this.drawPile.length();
	}
	
	public int calculatePoints(){
		if (this.centerCard.getValue() == this.playerCard.getValue()) {
			return 4;
		}
		else if (this.centerCard.getSuit().equals(this.playerCard.getSuit())){
			return 2;
		}
		else {
			return -1;
		}
	}
}